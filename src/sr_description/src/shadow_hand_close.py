#!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
from gazebo_msgs.srv import SetJointStates, SpawnEntity, DeleteModel
from generic_controller_plugin.srv import SetPIDParameters
from geometry_msgs.msg import Pose
#Shadow Right Hand fingers
shadow_hand_joint_dic={
                        "index":["rh_FFJ0","rh_FFJ1","rh_FFJ2","rh_FFJ3"],
                        "middle":["rh_MFJ0","rh_MFJ1","rh_MFJ2","rh_MFJ3"],
                        "ring":["rh_RFJ0","rh_RFJ1","rh_RFJ2","rh_RFJ3"],
                        "pinky":["rh_LFJ0","rh_LFJ1","rh_LFJ2","rh_LFJ3","rh_LFJ4"],
                        "thumb":["rh_THJ0","rh_THJ1","rh_THJ2","rh_THJ3","rh_THJ4"],
                        "wrist":["rh_WRJ0","rh_WRJ1"]
                      }



if __name__ == "__main__":

    # Shadow hand close
    rospy.wait_for_service("/shadowhand_motor/rh_MFJ2/set_target")
    joint_name = "rh_THJ2"
    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/" + joint_name + "/set_target", SetJointStates)
    resp_rh_MFJ1 = rh_MFJ1([0.1])
    joint_name = "rh_THJ3"
    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/" + joint_name + "/set_target", SetJointStates)
    resp_rh_MFJ1 = rh_MFJ1([1.1])
    joint_name = "rh_THJ4"
    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/" + joint_name + "/set_target", SetJointStates)
    resp_rh_MFJ1 = rh_MFJ1([-0.5])