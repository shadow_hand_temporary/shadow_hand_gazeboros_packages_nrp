#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
from std_msgs.msg import Float64

import random
shadow_hand_joint_dic={
                        "index":["rh_FFJ0","rh_FFJ1","rh_FFJ2","rh_FFJ3"],
                        "middle":["rh_MFJ0","rh_MFJ1","rh_MFJ2","rh_MFJ3"],
                        "ring":["rh_RFJ0","rh_RFJ1","rh_RFJ2","rh_RFJ3"],
                        "pinky":["rh_LFJ0","rh_LFJ1","rh_LFJ2","rh_LFJ3","rh_LFJ4"],
                        "thumb":["rh_THJ0","rh_THJ1","rh_THJ2","rh_THJ3","rh_THJ4"],
                        "wrist":["rh_WRJ0","rh_WRJ1"]
                      }


def talker():
    rh_joint = {} 
    for joint_name in shadow_hand_joint_dic["index"][0:3]:
        rh_joint[joint_name] = rospy.Publisher("/shadowhand_motor/" + joint_name + "/cmd_pos", Float64, queue_size=10)
    for joint_name in shadow_hand_joint_dic["middle"][0:3]:
        rh_joint[joint_name] = rospy.Publisher("/shadowhand_motor/" + joint_name + "/cmd_pos", Float64, queue_size=10)
    for joint_name in shadow_hand_joint_dic["ring"][0:3]:
        rh_joint[joint_name] = rospy.Publisher("/shadowhand_motor/" + joint_name + "/cmd_pos", Float64, queue_size=10)
    for joint_name in shadow_hand_joint_dic["pinky"][0:3]:
        rh_joint[joint_name] = rospy.Publisher("/shadowhand_motor/" + joint_name + "/cmd_pos", Float64, queue_size=10)

    for joint_name in shadow_hand_joint_dic["thumb"][0:4]:
        rh_joint[joint_name] = rospy.Publisher("/shadowhand_motor/" + joint_name + "/cmd_pos", Float64, queue_size=10)

    rate = rospy.Rate(100) # 10hz
    while not rospy.is_shutdown():
        for key,pub in rh_joint.items():
            #random_joint_angle = random.uniform(0, 1.57)
            if key =="rh_THJ4":
                random_joint_angle = random.uniform(-1,1)
            elif key == "rh_THJ3":
                random_joint_angle = random.uniform(0,1.22)
            elif key == "rh_THJ2":
                random_joint_angle = random.uniform(-0.20,0.20)
            elif key == "rh_THJ1":
                random_joint_angle = random.uniform(-0.52,0.52)
            elif key == "rh_THJ0":
                random_joint_angle = random.uniform(-1.57,0)
            else:
                random_joint_angle = random.uniform(0,1.57)
            rospy.loginfo(random_joint_angle)
            pub.publish(random_joint_angle)
            rate.sleep()

if __name__ == '__main__':
    rospy.init_node('talker', anonymous=True)
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
