cmake_minimum_required(VERSION 2.8.3)
project(gazebo_ros_replay_plugins)

set(CMAKE_CXX_STANDARD 11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rosbag
  gazebo_msgs
  std_srvs
  cle_ros_msgs
  message_generation
)

# Depend on system install of Gazebo
find_package(gazebo REQUIRED)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GAZEBO_CXX_FLAGS}")

find_package(Boost REQUIRED COMPONENTS filesystem)

add_service_files(
  FILES
  Start.srv
)


generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
  LIBRARIES
  gazebo_ros_recording_plugin
  gazebo_ros_playback_plugin

  CATKIN_DEPENDS
  roscpp
  rosbag
  gazebo_msgs
  std_srvs
  cle_ros_msgs
  message_runtime
)




include_directories(include
  ${Boost_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
  ${GAZEBO_INCLUDE_DIRS}
  ${TBB_INCLUDE_DIR}
)

link_directories(
  ${catkin_LIBRARY_DIRS}
)

set(cxx_flags)
foreach (item ${GAZEBO_CFLAGS})
  set(cxx_flags "${cxx_flags} ${item}")
endforeach ()

set(ld_flags)
foreach (item ${GAZEBO_LDFLAGS})
  set(ld_flags "${ld_flags} ${item}")
endforeach ()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GAZEBO_CXX_FLAGS}")

## Plugins
add_library(gazebo_ros_recording_plugin src/gazebo_ros_recording_plugin.cpp src/rosbag/recorder.cpp)
add_dependencies(gazebo_ros_recording_plugin ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
set_target_properties(gazebo_ros_recording_plugin PROPERTIES LINK_FLAGS "${ld_flags}")
set_target_properties(gazebo_ros_recording_plugin PROPERTIES COMPILE_FLAGS "${cxx_flags}")
target_link_libraries(gazebo_ros_recording_plugin ${GAZEBO_LIBRARIES} ${catkin_LIBRARIES} ${Boost_LIBRARIES})

add_library(gazebo_ros_playback_plugin src/gazebo_ros_playback_plugin.cpp src/rosbag/player.cpp)
add_dependencies(gazebo_ros_playback_plugin ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
set_target_properties(gazebo_ros_playback_plugin PROPERTIES LINK_FLAGS "${ld_flags}")
set_target_properties(gazebo_ros_playback_plugin PROPERTIES COMPILE_FLAGS "${cxx_flags}")
target_link_libraries(gazebo_ros_playback_plugin ${GAZEBO_LIBRARIES} ${catkin_LIBRARIES} ${Boost_LIBRARIES})

# Install Gazebo System Plugins
install(TARGETS gazebo_ros_recording_plugin gazebo_ros_playback_plugin
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
)
