#!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
from gazebo_msgs.srv import SetJointStates, SpawnEntity, DeleteModel
from generic_controller_plugin.srv import SetPIDParameters
from geometry_msgs.msg import Pose
#Shadow Right Hand fingers
shadow_hand_joint_dic={
                        "index":["rh_FFJ0","rh_FFJ1","rh_FFJ2","rh_FFJ3"],
                        "middle":["rh_MFJ0","rh_MFJ1","rh_MFJ2","rh_MFJ3"],
                        "ring":["rh_RFJ0","rh_RFJ1","rh_RFJ2","rh_RFJ3"],
                        "pinky":["rh_LFJ0","rh_LFJ1","rh_LFJ2","rh_LFJ3","rh_LFJ4"],
                        "thumb":["rh_THJ0","rh_THJ1","rh_THJ2","rh_THJ3","rh_THJ4"],
                        "wrist":["rh_WRJ0","rh_WRJ1"]
                      }




def delete_model():
    rospy.wait_for_service("/gazebo/delete_model")
    delete_model = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)
    resp_delete_model = delete_model("box")
    print(resp_delete_model)


def spawn_model():
    rospy.wait_for_service("/gazebo/spawn_sdf_entity")
    spawn_model = rospy.ServiceProxy("/gazebo/spawn_sdf_entity", SpawnEntity)
    entity_name="box"
    entity_xml="""
    <?xml version="1.0" ?>
    <sdf version='1.5'>
    <model name="box">
      <alllow_auto_disable>1</alllow_auto_disable>
        <pose>-0.02 0.358 0.516 0 0 0</pose>
      <link name="link">
      <pose frame=''>0 0 0 0 -0 0</pose>
      <inertial>
        <mass>0.1</mass>
        <inertia>
          <ixx>0.000026042</ixx>
          <ixy>0</ixy>
          <ixz>0</ixz>
          <iyy>0.000026042</iyy>
          <iyz>0</iyz>
          <izz>0.000026042</izz>
        </inertia>
      </inertial>
        <collision name="box_collision">
          <geometry>
            <!-- <box>
              <size>0.025 0.025 0.025</size>
            </box> -->
           <mesh>
            <scale>0.0125 0.0125 0.0125</scale>
            <uri>model://shadow_hand_cube_object/meshes/shadow_hand_cube_object.dae</uri>
          </mesh>
            <!-- <sphere>
             <radius>0.02</radius>
           </sphere> -->
          </geometry>
            <surface>
          <contact>
          <collide_without_contact_bitmask>1</collide_without_contact_bitmask>
          <ode>
          <soft_cfm>100.0</soft_cfm>
          <soft_erp> 100.0</soft_erp>
            <kp>1e5</kp>
            <kd>1e3</kd>
          </ode>
          <bullet>
            <friction>10</friction>
            <friction2>10</friction2>
            <fdir1>1 1 1</fdir1>
            <rolling_friction>10</rolling_friction>>
          </bullet>
        </contact>          
          <friction>
            <ode>
              <mu>100</mu>
              <mu2>100</mu2>
              <fdir1>1 1 1</fdir1>
              <slip1>0</slip1>
              <slip2>0</slip2>
            </ode>
          </friction>
          </surface> 

        </collision>

        <visual name="visual">
          <geometry>
           <mesh>
            <scale>0.0125 0.0125 0.0125</scale>
            <uri>model://shadow_hand_cube_object/meshes/shadow_hand_cube_object.dae</uri>
          </mesh>
          </geometry>
        </visual>
      <!-- <gravity>1</gravity>
    <velocity_decay>
      <linear>0.9</linear>
      <angular>0.9</angular>
      </velocity_decay> -->
       <sensor name='my_contact' type='contact'>
          <always_on>1</always_on>
        <update_rate>1000</update_rate>
        <contact>
          <collision>box_collision</collision>
          <topic>shadow_hand_box_contact</topic>
        </contact>
        </sensor>
      </link>
    </model>
    </sdf>"""
    entity_namespace=""
    initial_pose = Pose()
    initial_pose.position.x = 1.01
    initial_pose.position.y = 0.92
    initial_pose.position.z = 0.164
    reference_frame = "world"

    resp_spawn_model = spawn_model(entity_name,entity_xml,entity_namespace,initial_pose,reference_frame)
    print(resp_spawn_model)

if __name__ == "__main__":

    delete_model()
    spawn_model()
    
    rospy.wait_for_service("/shadowhand_motor/set_pid_parameters")
    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_MFJ1",30,10,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_MFJ2",30,10,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_MFJ0",30,10,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_RFJ1",30,10,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_RFJ2",30,10,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_RFJ0",30,10,0)
    print(resp_rh_MFJ1)


    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_LFJ0",50,20,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_LFJ1",50,20,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_LFJ2",50,20,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_LFJ4",50,20,0)
    print(resp_rh_MFJ1)


    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_FFJ0",30,10,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_FFJ1",30,10,0)
    print(resp_rh_MFJ1)

    rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/set_pid_parameters", SetPIDParameters)
    resp_rh_MFJ1= rh_MFJ1("shadowhand_motor::rh_FFJ2",30,10,0)
    print(resp_rh_MFJ1)







    # Shadow hand close
    # rospy.wait_for_service("/shadowhand_motor/rh_MFJ2/set_target")
    # for joint_name in shadow_hand_joint_dic["index"][0:3]:
    #     rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/" + joint_name + "/set_target", SetJointStates)
    #     resp_rh_MFJ1 = rh_MFJ1([1.57])
    # for joint_name in shadow_hand_joint_dic["middle"][0:3]:
    #     rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/" + joint_name + "/set_target", SetJointStates)
    #     resp_rh_MFJ1 = rh_MFJ1([1.57])
    # for joint_name in shadow_hand_joint_dic["ring"][0:3]:
    #     rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/" + joint_name + "/set_target", SetJointStates)
    #     resp_rh_MFJ1 = rh_MFJ1([1.57])
    # for joint_name in shadow_hand_joint_dic["pinky"][0:3]:
    #     rh_MFJ1 = rospy.ServiceProxy("/shadowhand_motor/" + joint_name + "/set_target", SetJointStates)
    #     resp_rh_MFJ1 = rh_MFJ1([1.57])