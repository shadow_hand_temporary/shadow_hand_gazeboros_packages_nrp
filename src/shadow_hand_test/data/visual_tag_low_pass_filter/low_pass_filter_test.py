import numpy as np
from scipy.signal import butter, lfilter, freqz
import matplotlib.pyplot as plt
import rospkg
import math
rospack = rospkg.RosPack()
path_of_shadow_hand_package = rospack.get_path('shadow_hand_test')


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y


# Filter requirements.
order = 6
fs = 10.0       # sample rate, Hz
cutoff = 0.5 # desired cutoff frequency of the filter, Hz

# Get the filter coefficients so we can check its frequency response.
b, a = butter_lowpass(cutoff, fs, order)

# Plot the frequency response.
w, h = freqz(b, a, worN=8000)
plt.subplot(3, 1, 1)

plt.subplot(3, 1, 1)
plt.plot(0.5*fs*w/np.pi, 20*np.log10(np.abs(h)), 'b', label='filter-order: '+ str(order))
plt.plot(cutoff, -3, 'ko',label='cut-off: '+ str(cutoff))
plt.axvline(cutoff, color='k')
plt.xlim(0, 0.5*fs)
plt.title("Lowpass Filter Frequency Response")
plt.xlabel('Frequency [Hz]')
plt.ylabel('Gain [dB]')

plt.legend()
plt.grid()

plt.subplot(3, 1, 2)
h_Phase = np.arctan2(np.imag(h),np.real(h))
plt.plot(0.5*fs*w/np.pi, h_Phase)
plt.axvline(cutoff, color='k')
plt.xlim(0, 0.5*fs)
plt.title("Lowpass Filter Frequency Response")
plt.xlabel('Frequency [Hz]')
plt.ylabel('Phase Angle [radian]')
plt.legend()
plt.grid()


# Demonstrate the use of the filter.
# First make some data to be filtered.
T = 5.0         # seconds
n = int(T * fs) # total number of samples
# t = np.linspace(0, T, n, endpoint=False)
# t = np.load(path_of_shadow_hand_package+"/data/timesteps.npy")

# "Noisy" data.  We want to recover the 1.2 Hz signal from this.
# data = np.sin(1.2*2*np.pi*t) + 1.5*np.cos(9*2*np.pi*t) + 0.5*np.sin(12.0*2*np.pi*t)
data = total_force_array = np.load(path_of_shadow_hand_package+ "/data/visual_tag_low_pass_filter/contact_names.npy",allow_pickle=True)

for k in data.item().keys():
    tmp_sum = sum(data.item().get(k))
    if tmp_sum != 0:
        print("Here non-zero key: ", k , "and the sum: ", tmp_sum)


# Filter the data, and plot both the original and filtered signals.

data = data.item().get("T_rfproximal_front_right_bottom")
t = range(0,len(data))
y = butter_lowpass_filter(data, cutoff, fs, order)

plt.subplot(3, 1, 3)
plt.plot(t, data, 'b-', label='data',alpha=0.3)
plt.plot(t, y, 'g-', linewidth=2, label='filtered data')
plt.xlabel('Timesteps')
plt.ylabel("$\sum$Total Force")
plt.grid()
plt.legend()

plt.subplots_adjust(hspace=0.35)
plt.show()

# np.save("/home/erdi/Desktop/test",y)